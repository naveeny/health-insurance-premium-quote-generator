package com.health.insurance.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import com.health.insurance.dto.HabitInformation;
import com.health.insurance.dto.HealthInformation;
import com.health.insurance.dto.UserDetails;
import com.health.insurance.dto.enums.Habit;
import com.health.insurance.dto.enums.Health;

public class TestInsuranceController {

	InsuranceController controller = new InsuranceController();

	/** Test validation where age is invalid */
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidAge() {

		UserDetails user = new UserDetails("Norman Gomes", "Male", 340,
				new HealthInformation(false, false, false, true),
				new HabitInformation(false, true, true, false));
		controller.calculatePremium(user);
	}

	/** Test actual scenario */
	@Test
	public void testCalculatePremium() {

		UserDetails user = new UserDetails("Norman Gomes", "Male", 34,
				new HealthInformation(false, false, false, true),
				new HabitInformation(false, true, true, false));

		assertEquals(String.valueOf(6850),
				String.format("%.0f", controller.calculatePremium(user)));

	}

	/** Test user premium calculation without any health issues */
	@Test
	public void testWithOutHealthIssues() {

		UserDetails user = new UserDetails("Norman Gomes", "Male", 20,
				new HealthInformation(false, false, false, false),
				new HabitInformation(false, false, false, false));

		assertEquals(String.valueOf(5610),
				String.format("%.0f", controller.calculatePremium(user)));

	}

	/** Test user premium calculation with health issues */
	@Test
	public void testWithHealthIssues() {

		UserDetails user = new UserDetails("Norman Gomes", "Male", 20,
				new HealthInformation(true, true, true, true),
				new HabitInformation(false, false, false, false));

		assertEquals(String.valueOf(5838),
				String.format("%.0f", controller.calculatePremium(user)));

	}

	/** Test user premium calculation with good habits */
	@Test
	public void testWithGoodHabits() {

		HabitInformation habits = new HabitInformation();
		habits.setAlcohol(false);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		habits.setSmoking(false);

		UserDetails user = new UserDetails("Norman Gomes", "Male", 20,
				new HealthInformation(true, true, true, true), habits);

		assertEquals(String.valueOf(5663),
				String.format("%.0f", controller.calculatePremium(user)));

	}

	/** Test user premium calculation with bad habits */
	@Test
	public void testWithBadHabits() {

		HabitInformation habits = new HabitInformation();
		habits.setAlcohol(true);
		habits.setDailyExercise(false);
		habits.setDrugs(true);
		habits.setSmoking(true);

		UserDetails user = new UserDetails("Norman Gomes", "Male", 20,
				new HealthInformation(true, true, true, true), habits);

		assertEquals(String.valueOf(6379),
				String.format("%.0f", controller.calculatePremium(user)));

	}

	/**
	 * Generic implementation --- Not yet complete - TODO
	 */
	public void testGeneric() {

		UserDetails user = new UserDetails("Norman Gomes", "Male", 34);
		user.setHabitList(new ArrayList<Habit>() {
			{
				add(Habit.ALCOHOL);
				add(Habit.DAILYEXERCISE);
			}
		});
		user.setHealthList(new ArrayList<Health>() {
			{
				add(Health.OVERWEIGHT);
			}
		});

		System.out.println("Amount is " + controller.calculatePremium(user));

	}
}
