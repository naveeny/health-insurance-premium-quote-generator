package com.health.insurance.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.health.insurance.dto.HabitInformation;
import com.health.insurance.dto.HealthInformation;
import com.health.insurance.dto.UserDetails;

public class TestHealthService {

	HealthService healthService = new HealthService();
	UserDetails user = new UserDetails("Norman Gomes", "Male", 35);

	@Before
	public void init() {
		HealthInformation health = new HealthInformation();

		health.setBloodPressure(false);
		health.setBloodSugar(false);
		health.setHypertension(false);
		health.setOverweight(false);
		user.setHealth(health);
	}

	@Test
	public void testHealthPremium() {
		assertEquals(String.valueOf(5000),
				String.format("%.0f", healthService.calculatePremium(user)));

	}
	
	@Test
	public void testHealthIssuePremium() {
		user.getHealth().setBloodPressure(true);
		assertEquals(String.valueOf(5050),
				String.format("%.0f", healthService.calculatePremium(user)));

	}
}
