package com.health.insurance.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.health.insurance.dto.UserDetails;

public class TestUserService {

	UserService userService = new UserService();
	UserDetails user = new UserDetails("Norman Gomes", "Male", 35);

	@Test
	public void testAgePremium() {
		user.setAge(34);

		userService.calculatePremium(user);
		assertEquals(String.valueOf(6788),
				String.format("%.0f", user.getAmount()));

	}

	@Test
	public void testAge18Premium() {
		user.setAge(18);
		userService.calculatePremium(user);
		assertEquals(String.valueOf(5610),
				String.format("%.0f", user.getAmount()));

	}

	@Test
	public void testAge25Premium() {
		user.setAge(25);
		userService.calculatePremium(user);
		assertEquals(String.valueOf(6171),
				String.format("%.0f", user.getAmount()));

	}

	@Test
	public void testAge35Premium() {
		user.setAge(35);
		userService.calculatePremium(user);
		assertEquals(String.valueOf(7467),
				String.format("%.0f", user.getAmount()));

	}

}