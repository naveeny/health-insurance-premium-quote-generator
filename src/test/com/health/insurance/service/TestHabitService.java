package com.health.insurance.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.health.insurance.dto.HabitInformation;
import com.health.insurance.dto.UserDetails;

public class TestHabitService {

	HabitService habitService = new HabitService();
	UserDetails user = new UserDetails("Norman Gomes", "Male", 35);

	@Before
	public void init() {
		HabitInformation habits = new HabitInformation();

		habits.setAlcohol(false);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		habits.setSmoking(false);
		user.setHabits(habits);
	}

	@Test
	public void testGoodHabitPremium() {
		assertEquals(String.valueOf(4850),
				String.format("%.0f", habitService.calculatePremium(user)));

	}
	
	@Test
	public void testBadHabitPremium() {
		user.getHabits().setDailyExercise(false);
		user.getHabits().setAlcohol(true);
		assertEquals(String.valueOf(5150),
				String.format("%.0f", habitService.calculatePremium(user)));

	}
}
