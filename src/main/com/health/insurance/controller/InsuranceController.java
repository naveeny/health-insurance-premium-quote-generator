package com.health.insurance.controller;

import com.health.insurance.dto.UserDetails;
import com.health.insurance.service.*;

public class InsuranceController {

	public double calculatePremium(UserDetails user) {
		PremiumCalculatorService service = new PremiumCalculatorService();
		return service.calculatePremium(user);
	}

}
