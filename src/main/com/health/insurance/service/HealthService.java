package com.health.insurance.service;

import com.health.insurance.dto.HealthInformation;
import com.health.insurance.dto.UserDetails;
import com.health.insurance.dto.enums.Health;

public class HealthService implements PremiumStrategy {

	/**
	 * get individual incrementValue
	 * 
	 * @param health
	 * @param amount
	 * @return
	 */
	public static double calculatePremium(HealthInformation health,
			double amount) {

		if (health.isBloodPressure()) {
			amount = amount * Health.BLOOD_PRESSURE.getIncrementValue();
		}

		if (health.isBloodSugar()) {
			amount = amount * Health.BLOOD_SUGAR.getIncrementValue();
		}

		if (health.isHypertension()) {
			amount = amount * Health.HYPER_TENSION.getIncrementValue();
		}

		if (health.isOverweight()) {
			amount = amount * Health.OVERWEIGHT.getIncrementValue();
		}

		return amount;
	}

	@Override
	public double calculatePremium(UserDetails user) {
		return calculatePremium(user.getHealth(), user.getAmount());
	}

}
