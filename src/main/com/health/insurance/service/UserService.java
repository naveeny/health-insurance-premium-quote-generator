package com.health.insurance.service;

import com.health.insurance.dto.UserDetails;
import com.health.insurance.dto.enums.Gender;

public class UserService implements PremiumStrategy {

	@Override
	public double calculatePremium(UserDetails user) {

		/** calculatePremium based on Age information */
		user.setAmount(ageCalculator(user.getAge(), user.getAmount()));

		/** calculatePremium based on Gender information */
		user.setAmount(genderCalculator(user.getGender(), user.getAmount()));

		return user.getAmount();
	}

	private static double genderCalculator(String gender, double amount) {
		if (Gender.MALE.getGender().equals(gender)) {
			amount = amount * Gender.MALE.getIncrementValue();
		} else {
			/** if any increment for other genders */
		}
		return amount;
	}

	/**
	 * Calculate Premium based on Age
	 * 
	 * @param age
	 * @param amount
	 * @return
	 */
	private static double ageCalculator(int age, double amount) {

		if (age < 25) {
			amount = amount * 1.1;
		} else if (age < 30) {
			amount = (amount * 1.1) * 1.1;
		} else if (age < 35) {
			amount = ((amount * 1.1) * 1.1) * 1.1;
		} else if (age < 40) {
			amount = (((amount * 1.1) * 1.1) * 1.1) * 1.1;
		} else {
			amount = (((amount * 1.1) * 1.1) * 1.1) * 1.1;
			int value = (age - 40) / 5;
			for (int i = 0; i < value; i++) {
				amount = amount * 1.2;
			}
		}

		return amount;
	}
}
