package com.health.insurance.service;

import java.util.ArrayList;
import java.util.List;

import com.health.insurance.dto.UserDetails;
import com.health.insurance.service.helper.UserDetailsValidator;
import com.health.insurance.service.helper.UserValidator;

public class PremiumCalculatorService {

	public double calculatePremium(UserDetails user) {

		/** validate the user request */
		List<UserDetailsValidator> rules = new ArrayList<>();
		rules.add(new UserValidator());

		/**
		 * Find request error and throw approp error messages rules.add(new
		 * HealthValidator()); rules.add(new HabitValidator());
		 */

		for (UserDetailsValidator rule : rules) {
			rule.validate(user);
		}

		/** calculatePremium based on User information */
		calculate(new UserService(), user);

		/** calculatePremium based on Health information */
		calculate(new HealthService(), user);

		/** calculatePremium based on Habit information */
		calculate(new HabitService(), user);

		return user.getAmount();
	}

	public void calculate(PremiumStrategy premiumStrategy, UserDetails user) {
		user.setAmount(premiumStrategy.calculatePremium(user));
	}

}
