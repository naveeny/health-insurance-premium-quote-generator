package com.health.insurance.service;

import com.health.insurance.dto.HabitInformation;
import com.health.insurance.dto.UserDetails;
import com.health.insurance.dto.enums.Habit;

public class HabitService implements PremiumStrategy {

	/**
	 * get individual incrementValue
	 * 
	 * @param habits
	 * @param amount
	 * @return
	 */
	public static double calculate(HabitInformation habits, double amount) {

		if (habits.isDrugs()) {
			amount = amount * Habit.DRUGS.getIncrementValue();
		}

		if (habits.isAlcohol()) {
			amount = amount * Habit.ALCOHOL.getIncrementValue();
		}

		if (habits.isDailyExercise()) {
			amount = amount * Habit.DAILYEXERCISE.getIncrementValue();
		}

		if (habits.isSmoking()) {
			amount = amount * Habit.SMOKING.getIncrementValue();
		}

		return amount;
	}

	@Override
	public double calculatePremium(UserDetails user) {
		return calculate(user.getHabits(), user.getAmount());
	}
}
