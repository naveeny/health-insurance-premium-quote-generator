package com.health.insurance.service.helper;

import com.health.insurance.dto.UserDetails;

public interface UserDetailsValidator {

	void validate(UserDetails userDetails);
}
