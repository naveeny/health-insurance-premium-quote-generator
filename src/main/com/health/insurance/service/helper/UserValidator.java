package com.health.insurance.service.helper;

import com.health.insurance.dto.UserDetails;

public class UserValidator implements UserDetailsValidator {

	@Override
	public void validate(UserDetails userDetails) {
		if (userDetails.getAge() < 0 || userDetails.getAge() > 150) {
			throw new IllegalArgumentException("Age is not a valid !");
		}
	}

}
