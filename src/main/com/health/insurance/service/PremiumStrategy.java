package com.health.insurance.service;

import com.health.insurance.dto.UserDetails;

public interface PremiumStrategy {
	double calculatePremium(UserDetails user);
}
