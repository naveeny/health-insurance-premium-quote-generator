package com.health.insurance.service;

import com.health.insurance.dto.UserDetails;

public interface PremiumCalculator {
	double calculatePremium(UserDetails user);
}
