package com.health.insurance;

import java.util.Scanner;

import com.health.insurance.controller.InsuranceController;
import com.health.insurance.dto.HabitInformation;
import com.health.insurance.dto.HealthInformation;
import com.health.insurance.dto.UserDetails;

public class HealthInsuranceApplication {

	public static void main(String[] args) {
		InsuranceController controller = new InsuranceController();
		UserDetails user = new UserDetails();
		user.setName("Norman Gomes");
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter age ");
		user.setAge(scanner.nextInt());
		System.out.println("Enter Gender ");
		user.setGender(scanner.next());
		HabitInformation habits = new HabitInformation();
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		habits.setSmoking(false);

		user.setHabits(habits);
		user.setHealth(new HealthInformation(false, false, false, true));

		System.out.print("Health Insurance Premium for " +user.getName() +  " : Rs. "+String.format("%.0f",
				controller.calculatePremium(user)));
	}
}
