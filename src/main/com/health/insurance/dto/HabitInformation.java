package com.health.insurance.dto;

public class HabitInformation {

	public boolean smoking;

	public boolean alcohol;

	public boolean dailyExercise;

	public boolean drugs;

	public HabitInformation(boolean smoking, boolean alcohol,
			boolean dailyExercise, boolean drugs) {
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;
	}

	public HabitInformation() {
		// TODO Auto-generated constructor stub
	}

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}

	public boolean isAlcohol() {
		return alcohol;
	}

	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}

	public boolean isDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public boolean isDrugs() {
		return drugs;
	}

	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}

	public boolean isBadHabit() {

		if (this.alcohol || this.drugs || this.smoking) {
			return true;
		}
		return false;
	}

}
