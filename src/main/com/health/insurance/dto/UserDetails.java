package com.health.insurance.dto;

import java.util.List;

import com.health.insurance.dto.enums.Habit;
import com.health.insurance.dto.enums.Health;

public class UserDetails {

	public String name;

	public String gender;

	public int age;

	public HealthInformation health;

	public HabitInformation habits;

	public double amount = 5000.00;

	List<Health> healthList;

	List<Habit> habitList;

	public UserDetails(String name, String gender, int age,
			HealthInformation health, HabitInformation habits) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.health = health;
		this.habits = habits;
	}

	public UserDetails(String name, String gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	public UserDetails() {
		// TODO Auto-generated constructor stub
	}

	public List<Health> getHealthList() {
		return healthList;
	}

	public void setHealthList(List<Health> healthList) {

		this.healthList = healthList;
	}

	public List<Habit> getHabitList() {
		return habitList;
	}

	public void setHabitList(List<Habit> habitList) {
		this.habitList = habitList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public HealthInformation getHealth() {
		return health;
	}

	public void setHealth(HealthInformation health) {
		this.health = health;
	}

	public HabitInformation getHabits() {
		return habits;
	}

	public void setHabits(HabitInformation habits) {
		this.habits = habits;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
