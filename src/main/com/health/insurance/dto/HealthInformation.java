package com.health.insurance.dto;

import java.util.ArrayList;
import java.util.List;

import com.health.insurance.dto.enums.Health;

public class HealthInformation {

	public List<Health> healthList = new ArrayList<>();

	public boolean hypertension;

	public boolean bloodPressure;

	public boolean bloodSugar;

	public boolean overweight;

	public HealthInformation(boolean hypertension, boolean bloodPressure,
			boolean bloodSugar, boolean overweight) {
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overweight = overweight;
	}

	public HealthInformation() {
		// TODO Auto-generated constructor stub
	}

	public boolean isHypertension() {
		return hypertension;
	}

	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}

	public boolean isBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public boolean isBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public boolean isOverweight() {
		return overweight;
	}

	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}

	public int getHealthCount() {
		int count = 0;

		if (this.bloodPressure) {
			count++;
		}

		if (this.bloodSugar) {
			count++;
		}

		if (this.hypertension) {
			count++;
		}

		if (this.overweight) {
			count++;
		}

		return count;
	}

}
