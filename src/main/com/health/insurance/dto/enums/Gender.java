package com.health.insurance.dto.enums;

public enum Gender {
	MALE("Male", 1.02),

	FEMALE("Female", 1.00),

	OTHER("Other", 1.00);

	public String gender;

	public double incrementValue;

	Gender(String gender) {
		this.gender = gender;
	}

	Gender(String gender, double incrementValue) {
		this.gender = gender;
		this.incrementValue = incrementValue;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getIncrementValue() {
		return incrementValue;
	}

	public void setIncrementValue(double incrementValue) {
		this.incrementValue = incrementValue;
	}

}
