package com.health.insurance.dto.enums;

/**
 * User Habit information having key and incrementValue
 * 
 * @author user1
 * 
 */
public enum Habit {

	SMOKING("smoking", 1.03),

	ALCOHOL("alcohol", 1.03),

	DAILYEXERCISE("dailyExercise", 0.97),

	DRUGS("drugs", 1.03);

	public String habit;

	public double incrementValue;

	Habit(String habit, double incrementValue) {
		this.habit = habit;
		this.incrementValue = incrementValue;
	}

	Habit(String habit) {
		this.habit = habit;
	}

	public String getHabit() {
		return habit;
	}

	public void setHabit(String habit) {
		this.habit = habit;
	}

	public double getIncrementValue() {
		return incrementValue;
	}

	public void setIncrementValue(double incrementValue) {
		this.incrementValue = incrementValue;
	}

}
