package com.health.insurance.dto.enums;

/**
 * User Health Information having key and incrementValue
 * 
 * @author user1
 * 
 */
public enum Health {

	HYPER_TENSION("hypertension", 1.01),

	BLOOD_PRESSURE("bloodPressure", 1.01),

	BLOOD_SUGAR("bloodSugar", 1.01),

	OVERWEIGHT("overweight", 1.01);

	public String health;

	public double incrementValue;

	Health(String health, double incrementValue) {
		this.health = health;
		this.incrementValue = incrementValue;
	}

	Health(String health) {
		this.health = health;
	}

	public String getHealth() {
		return health;
	}

	public void setHealth(String health) {
		this.health = health;
	}

	public double getIncrementValue() {
		return incrementValue;
	}

	public void setIncrementValue(double incrementValue) {
		this.incrementValue = incrementValue;
	}

}
